import { cleanup, screen, render } from '@testing-library/react'
import renderer, {create} from 'react-test-renderer'
import PasswordInput from '.'

afterEach(() => {
    cleanup()
})


test('update password', () => {
    let password = renderer.create(<PasswordInput />)
    expect(password.toJSON()).toMatchSnapshot()

})