

export default function Navbar() {
    return (
        <div 
        className="navbar-item" 
        data-testid="navbar-1" 
        style={{
            display : 'flex',
            backgroundColor : 'red'
        }}
        >
            <ul>
                <li>
                    <a href="/home">Home</a>
                </li>
                <li>
                    <a href="/about">About</a>
                </li>
            </ul>

            <button 
            className="btn btn-danger button" 
            data-testid="button-logout" 
            >Logout</button>
        </div>
    )
}