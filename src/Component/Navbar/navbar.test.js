import { cleanup, screen, render } from '@testing-library/react'
import Navbar from '.'
import renderer from 'react-test-renderer'


afterEach(() => {
    cleanup()
})


test("Check Warna dari button logout", () => {
    render(<Navbar />)
    const buttonLogout = screen.getByTestId("button-logout")
    expect(buttonLogout).toHaveClass("btn btn-danger")
})

test("Check tampilan navbar", () => {
    render(<Navbar />)
    const navbarContainer = screen.getByTestId("navbar-1")
    expect(navbarContainer).toHaveStyle('display: flex')
})

test("snapshot renderinng", () => {
    const navbarSnanShoot = renderer.create(<Navbar />).toJSON()
    expect(navbarSnanShoot).toMatchSnapshot()
    expect(navbarSnanShoot.type).toBe('div')
})
