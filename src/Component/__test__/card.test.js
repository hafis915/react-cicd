import TestRenderer from 'react-test-renderer';
import { cleanup, screen, render } from '@testing-library/react'
import Card from '../card'

afterEach( () => {
    cleanup()
})

test('Rendering Card', () => {
    render(<Card />)
    const testCard = screen.getByTestId('card-div-test')
    expect(testCard).toBeInTheDocument()
    expect(testCard).toHaveTextContent("HELLO")
})

test("rendering subdiv", () => {
    render(<Card />)
    const subdiv = screen.getByTestId('card-subdiv-1')
    const element = screen.getByTestId("el-1")
    expect(subdiv).toBeInTheDocument()
    expect(subdiv).toHaveTextContent("heelo")
    expect(subdiv).toContainElement(element)
    expect(subdiv).toContainHTML('<p>heelo <span>*</span> </p>')

})

