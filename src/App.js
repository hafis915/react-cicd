import Navbar from './Component/Navbar';
import Card from './Component/card';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Card />

    </div>
  );
}

export default App;
